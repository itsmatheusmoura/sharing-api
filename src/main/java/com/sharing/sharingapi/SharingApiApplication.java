package com.sharing.sharingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SharingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SharingApiApplication.class, args);
	}

}
