package com.sharing.sharingapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/sharing")
public class SharingController {

    @GetMapping
    public String teste(){
        return "Hello World";
    }

    @GetMapping("/soma")
    public List<Integer> teste2(){

        int i = 0;
        List numerosPrimos = new ArrayList<>();

        while(i < 10000){
            boolean resposta = ehPrimo(i);
            if(resposta){
                numerosPrimos.add(i);
            }
            i++;
        }
        return numerosPrimos;
    }

    private static boolean ehPrimo(int numero) {
        for (int j = 2; j < numero; j++) {
            if (numero % j == 0)
                return false;
        }
        return true;
    }

}
